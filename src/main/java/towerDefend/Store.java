package towerDefend;

import java.awt.*;

public class Store {
    public static int shopWidth = 8; // задем количество ячеек в магазине
    public static int buttonSize = 52; //задаем размер ячейки
    public static int cellSpace = 2; //растояние между ячейками
    public static int awayFromRoom = 29; //растояние от экрана к ячейке
    public static int iconSize = 20; //задаем размер иконки здоровья и монет
    public static int iconSpace = 6; // растояние между  иконки здоровья и монет
    public static int iconTextY = 15; // сдвиг текста по оси Y
    public static int itemIn = 4;
    public static int heldID = -1;
    public static int realID = -1;
    public static int[] buttonID = {Value.airTowerLaser, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airTrashCan};
    public static int[] buttonPrice = {10, 0, 0, 0, 0, 0, 0, 0};


    public Rectangle[] button = new Rectangle[shopWidth]; // создаем массив магазинна
    public Rectangle buttonHealth; // создаем прямоугольник для здоровья
    public Rectangle buttonCoins; // создаем прямоугольник для монет

    public boolean holdsItem = false;

    public Store() {
        define(); // при создании обэкта вызывается метод define()
    }

    public void click(int mouseButton) {
        if (mouseButton == 1) {
            for (int i = 0; i < button.length; i++) {
                if (button[i].contains(Screen.mse)) {
                    if (buttonID[i] != Value.airAir) {
                        if (buttonID[i] == Value.airTrashCan) { // delete item
                            holdsItem = false;
                        } else {
                            heldID = buttonID[i];
                            realID = i;
                            holdsItem = true;
                        }
                    }
                }
            }
            if (holdsItem) {
                if (Screen.coinge >= buttonPrice[realID]){
                    for (int y = 0; y < Screen.room.block.length; y++) {
                        for (int x = 0; x < Screen.room.block[0].length; x++) {
                            if (Screen.room.block[y][x].contains(Screen.mse)){
                                if (Screen.room.block[y][x].groundID != Value.groundRoad && Screen.room.block[y][x].airID == Value.airAir){
                                    Screen.room.block[y][x].airID = heldID;
                                    Screen.coinge -= buttonPrice[realID];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void define() { //в методете определям положение и создаем ячейки
        for (int i = 0; i < button.length; i++) {
            button[i] = new Rectangle((Screen.myWidth / 2) - ((shopWidth * (buttonSize + cellSpace)) / 2) + ((buttonSize + cellSpace) * i),
                    (Screen.room.block[Screen.room.worldHeight - 1][0].y) + Screen.room.blockSize + awayFromRoom, buttonSize, buttonSize);
            // создаем в массиве каждую ячейку отдельно, где: 1 - координита Х, 2 - координита У, 3 - длина, 4- ширина
        }

        buttonHealth = new Rectangle(Screen.room.block[0][0].x - 1, button[0].y, iconSize, iconSize); //создаем иконку здоровья на экране
        buttonCoins = new Rectangle(Screen.room.block[0][0].x - 1, button[0].y + button[0].height - iconSize, iconSize, iconSize); //создаем иконку здоровья на экране
    }

    public void draw(Graphics g) { // прорисовываем ячейки
        for (int i = 0; i < button.length; i++) {
            if (button[i].contains(Screen.mse)) {  // если ячейка содержит мышку
                g.setColor(new Color(255, 255, 255, 100)); // устанавливаем новый цвет
                g.fillRect(button[i].x, button[i].y, button[i].width, button[i].height); // для текущей ячейки, кака наналог HTML:hover
            }

            g.drawImage(Screen.tileset_res[0], button[i].x, button[i].y, button[i].width, button[i].height, null); // добавляем заятавку ячейкам
            if (buttonID[i] != Value.airAir) {
                g.drawImage(Screen.tileset_air[buttonID[i]], button[i].x + itemIn, button[i].y + itemIn, button[i].width - (itemIn * 2), button[i].height - (itemIn * 2), null);
            }
            if (buttonPrice[i] > 0) {
                g.setFont(new Font("Courier New", Font.BOLD, 14));
                g.setColor(new Color(255, 255, 255));
                g.drawString("$" + buttonPrice[i], button[i].x + itemIn, button[i].y + itemIn + 10);
            }
        }

        g.drawImage(Screen.tileset_res[1], buttonHealth.x, buttonHealth.y, buttonHealth.width, buttonHealth.height, null); // прорисовываем ячейку здоровья на экране
        g.drawImage(Screen.tileset_res[2], buttonCoins.x, buttonCoins.y, buttonCoins.width, buttonCoins.height, null); // прорисовываем ячейку монет на экране
        g.setFont(new Font("Courier New", Font.BOLD, 14));
        g.setColor(new Color(255, 255, 255));
        g.drawString("" + Screen.health, buttonHealth.x + buttonHealth.width + iconSpace, buttonHealth.y + iconTextY);
        g.drawString("" + Screen.coinge, buttonCoins.x + buttonCoins.width + iconSpace, buttonCoins.y + iconTextY);

        if (holdsItem) {
            g.drawImage(Screen.tileset_air[heldID], Screen.mse.x - ((button[0].width - (itemIn * 2)) / 2) + itemIn, Screen.mse.y - ((button[0].width - (itemIn * 2)) / 2) + itemIn, button[0].width - (itemIn * 2), button[0].height - (itemIn * 2), null);
        }
    }
}
