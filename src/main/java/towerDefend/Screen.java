package towerDefend;

import towerDefend.minions.entitiesMinions.AbstractMinion;
import towerDefend.minions.entitiesMinions.mobs.EnemyRegular;
import towerDefend.minions.minionActions.MobActions;
import towerDefend.minions.minionActions.actions.MinionMove;

import java.awt.*;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import javax.swing.*;
import java.io.*;

public class Screen extends JPanel implements Runnable {
    public Thread thread = new Thread(this);

    public static Image[] tileset_ground = new Image[100];
    public static Image[] tileset_air = new Image[100];
    public static Image[] tileset_res = new Image[100];
    public static Image[] tileset_mob = new Image[100];

    public static int myWidth, myHeight;
    public static int coinge = 10, health = 100;
    public static int killed = 0, killToWin = 0, level = 1, maxLevel = 3;
    public static int winTime = 4000, winFrame = 0;

    public static boolean isFirst = true;
    public static boolean isDebug = false;
    public static boolean isWin = false;


    public static Point mse = new Point(0, 0);

    public static Room room;
    public static Save save;
    public static Store store;

    public static AbstractMinion[] mobs = new AbstractMinion[10];

    public Screen(Frame frame) {
        frame.addMouseListener(new KeyHandel());
        frame.addMouseMotionListener(new KeyHandel());

        thread.start();

    }

    public static void hasWon() {
        if (killed == killToWin) {
            isWin = true;
            killed = 0;
            coinge = 10;
            health = 100;
        }
    }

    public void define() {
        room = new Room();
        save = new Save();
        store = new Store();


        for (int i = 0; i < tileset_ground.length; i++) {
            tileset_ground[i] = new ImageIcon("src/main/resources/tileset_ground2.png").getImage();
            tileset_ground[i] = createImage(new FilteredImageSource(tileset_ground[i].getSource(), new CropImageFilter(0, 26 * i, 26, 26)));
        }
        for (int i = 0; i < tileset_air.length; i++) {
            tileset_air[i] = new ImageIcon("src/main/resources/tileset_air.png").getImage();
            tileset_air[i] = createImage(new FilteredImageSource(tileset_air[i].getSource(), new CropImageFilter(0, 26 * i, 26, 26)));
        }

        tileset_res[0] = new ImageIcon("src/main/resources/cell.png").getImage(); // добавляем в масив картинок изображение ячейки магазина под индекс 0
        tileset_res[1] = new ImageIcon("src/main/resources/heart.png").getImage(); // добавляем в масив картинок изображение сердца магазина под индекс 1
        tileset_res[2] = new ImageIcon("src/main/resources/coin.png").getImage(); // добавляем в масив картинок изображение монеты магазина под индекс 2

        tileset_mob[0] = new ImageIcon("src/main/resources/mob.png").getImage(); // добавляем в масив картинку моба

        save.loadSave(new File("src/main/Save/mission" + level + ".ulixava"));

        for (int i = 0; i < mobs.length; i++) {
            mobs[i] = new EnemyRegular();
        }
    }

    public void paintComponent(Graphics g) {
        if (isFirst) {
            myWidth = getWidth();
            myHeight = getHeight();
            define();

            isFirst = false;
        }

        g.setColor(new Color(50, 50, 50)); // устанавливаем цвет заставки за полем
        g.fillRect(0, 0, getWidth(), getHeight()); // рисуем все окно
        g.setColor(new Color(0, 0, 0)); //  устанавливаем цвет линии
        g.drawLine(room.block[0][0].x - 1, 0, room.block[0][0].x - 1, room.block[room.worldHeight - 1][0].y + room.blockSize); // рисую левую линию
        g.drawLine(room.block[0][room.worldWidth - 1].x + room.blockSize, 0, room.block[0][room.worldWidth - 1].x + room.blockSize, room.block[room.worldHeight - 1][0].y + room.blockSize); // рисую правую линию
        g.drawLine(room.block[0][0].x, room.block[room.worldHeight - 1][0].y + room.blockSize, room.block[0][room.worldWidth - 1].x + room.blockSize, room.block[room.worldHeight - 1][0].y + room.blockSize); // рисую нижнюю линию

        room.draw(g); //Drawing the room.

        for (int i = 0; i < mobs.length; i++) {
            if (mobs[i].inGame) {
                mobs[i].draw(g);

            }
        }

        store.draw(g); // Рисуем на экране магазин

        if (health < 1) {
            g.setColor(new Color(240, 20, 20));
            g.fillRect(0, 0, myWidth, myHeight);
            g.setFont(new Font("Courier New", Font.BOLD, 14));
            g.setColor(new Color(255, 255, 255));
            g.drawString("Game over", 10, 10);
        }

        if (isWin) {
            g.setColor(new Color(255, 255, 255));
            g.fillRect(0, 0, myWidth, myHeight);
            g.setFont(new Font("Courier New", Font.BOLD, 14));
            g.setColor(new Color(0, 0, 0));
            if (level == maxLevel) {
                g.drawString("You won the whole game", 10, 10);
            } else {
                g.drawString("You won! Please wait for the next level...", 10, 10);
            }
        }
    }

    public int spanTime = 400, spanFrame = 0;

    public void mobSpawner() {
        if (spanFrame > spanTime) {
            for (int i = 0; i < mobs.length; i++) {
                if (!mobs[i].inGame) {
                    mobs[i].spawnMob(Value.mobGreeny);
                    break;
                }
            }
            spanFrame = 0;
        } else {
            spanFrame += 1;
        }

    }

    public Thread thr = new Thread(new myThred());

    public void run() {
        thr.start();
        while (true) {
            if (!isFirst && health > 0 && !isWin) {

                room.physic();
                mobSpawner();
                /*for (int i = 0; i < mobs.length; i++) {
                    if (mobs[i].inGame) {
                        mobs[i].physic();
                    }
                }*/
            } else {
                if (isWin) {
                    if (winFrame == winTime) {
                        if (level == maxLevel) {
                            System.exit(0);
                        } else {
                            isWin = false;
                            level += 1;
                            define();
                            isWin = false;
                        }
                        winFrame = 0;

                    } else {
                        winFrame += 1;
                    }

                }
            }

            repaint();

            try {
                Thread.sleep(1);
            } catch (Exception e) {

            }
        }
    }

    public class myThred implements Runnable {
        public void run() {
            try {
                while (true) {
                    for (int i = 0; i < mobs.length; i++) {
                        if (mobs[i] != null) {
                            if (mobs[i].inGame) {
                                MobActions.minionMove(mobs[i]);
                            }
                        }
                    }
                    thread.sleep(40);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
