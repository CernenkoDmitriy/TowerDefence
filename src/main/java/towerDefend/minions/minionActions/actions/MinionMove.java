package towerDefend.minions.minionActions.actions;

import towerDefend.Screen;
import towerDefend.Value;
import towerDefend.minions.entitiesMinions.AbstractMinion;

public class MinionMove{
       public static void minionMove (AbstractMinion mob){

           if (mob.direction == mob.right) {
               mob.x += 1;
           } else if (mob.direction == mob.upward) {
               mob.y -= 1;
           } else if (mob.direction == mob.downward) {
               mob.y += 1;
           } else if (mob.direction == mob.left) {
               mob.x -= 1;
           }

           mob.mobWalk += 1;
           if (mob.mobWalk == Screen.room.blockSize) {
               if (mob.direction == mob.right) {
                   mob.xC += 1;
                   mob.hasRight = true;
               } else if (mob.direction == mob.upward) {
                   mob.yC -= 1;
                   mob.hasUpward = true;
               } else if (mob.direction == mob.downward) {
                   mob.yC += 1;
                   mob.hasDownward = true;
               } else if (mob.direction == mob.left) {
                   mob.xC -= 1;
                   mob.hasLeft = true;
               }

               if (!mob.hasUpward) {
                   try {
                       if (Screen.room.block[mob.yC + 1][mob.xC].groundID == Value.groundRoad) {
                           mob.direction = mob.downward;
                       }
                   } catch (Exception e) {
                   }
               }
               if (!mob.hasDownward) {
                   try {
                       if (Screen.room.block[mob.yC - 1][mob.xC].groundID == Value.groundRoad) {
                           mob.direction = mob.upward;
                       }
                   } catch (Exception e) {
                   }
               }
               if (!mob.hasLeft) {
                   try {
                       if (Screen.room.block[mob.yC][mob.xC + 1].groundID == Value.groundRoad) {
                           mob.direction = mob.right;
                       }
                   } catch (Exception e) {
                   }
               }

               if (!mob.hasRight) {
                   try {
                       if (Screen.room.block[mob.yC][mob.xC - 1].groundID == Value.groundRoad) {
                           mob.direction = mob.left;
                       }
                   } catch (Exception e) {
                   }
               }

               if(Screen.room.block[mob.yC][mob.xC].airID == Value.airCave){
                   mob.deleteMob();
                   mob.looseHealth();
               }

               mob.hasDownward = false;
               mob.hasUpward = false;
               mob.hasLeft = false;
               mob.hasRight = false;
               mob.mobWalk = 0;
           }

       }
}
