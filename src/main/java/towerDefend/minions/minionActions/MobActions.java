package towerDefend.minions.minionActions;

import towerDefend.minions.entitiesMinions.AbstractMinion;
import towerDefend.minions.minionActions.actions.MinionMove;

public class MobActions {

    public static void minionSpawn (AbstractMinion mob){};

    public static void minionMove(AbstractMinion mob) {
        MinionMove.minionMove(mob);
    }

    public static boolean isMinionAlive (AbstractMinion mob){
        if (mob.health <=0){
            return false;
        }else {
            return true;
        }
    };
}
