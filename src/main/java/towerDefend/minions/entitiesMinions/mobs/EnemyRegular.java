package towerDefend.minions.entitiesMinions.mobs;

import towerDefend.minions.entitiesMinions.AbstractMinion;

public class EnemyRegular extends AbstractMinion{
    public int health;
    public int mobSize = 26; // размер мобов

    public EnemyRegular (){
        super();
        health = 100;
    }
}
