package towerDefend.minions.entitiesMinions;

import towerDefend.Screen;
import towerDefend.Value;

import java.awt.*;

public abstract class AbstractMinion extends Rectangle{

    public int startHealth;

    public boolean isAlive = false;

    public int health;
    public int mobSize = 26; // размер мобов
    public int healthSpace = 3, healthHight = 6;
    public int mobWalk = 0;
    public int upward = 0, downward = 1, right = 2, left = 3;
    public int direction = right;
    public int mobID = Value.mobAir;
    public boolean inGame = false;
    public int xC, yC;

    public boolean hasUpward = false;
    public boolean hasDownward = false;
    public boolean hasLeft = false;
    public boolean hasRight = false;

    public void deleteMob() {
        inGame = false;
        direction = right;
        mobWalk = 0;
        Screen.room.block[0][0].getMoney(mobID);
        Screen.killed += 1;
    }


    public void looseHealth() {
        Screen.health -= 1;
    }

    public int zz;
    public void spawnMob(int mobID) { // призыв мобов
        for (int i = 0; i < Screen.room.block.length; i++) {
            if (Screen.room.block[i][0].groundID == Value.portal) {
                setBounds(Screen.room.block[i][0].x, Screen.room.block[i][0].y , mobSize, mobSize);

                xC = 0;
                yC = i;
                zz = ((int)(Math.random()*30));
            }
        }

        this.mobID = mobID;

        this.health = mobSize;

        inGame = true;


    }

    public void loseHealth(int amo) {
        health -= amo;
        checkDeath();
    }

    public void checkDeath() {
        if (health <= 0) {
            deleteMob();
        }
    }

    public boolean isDead() {
        if (inGame) {
            return false;
        } else {
            return true;
        }
    }

    public void draw(Graphics g) {
        g.drawImage(Screen.tileset_mob[mobID], x +zz, y+zz, width, height, null);

        // health bar
        g.setColor(new Color(180, 50, 50));
        g.fillRect(x+zz, y - (healthSpace + healthHight) +zz, width, healthHight);

        g.setColor(new Color(50, 180, 50));
        g.fillRect(x+zz, y - (healthSpace + healthHight) +zz, health, healthHight);

        g.setColor(new Color(0, 0, 0));
        g.drawRect(x+zz, y - (healthSpace + healthHight) +zz, health - 1 , healthHight - 1);
    }

}
